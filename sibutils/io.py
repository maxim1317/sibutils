"""Tools for reading and writing to files."""
import hashlib


def json_to_dict(json_path: str) -> dict:
    """Read json file and returns dictionary.

    Args:
        json_path (str): Path to JSON file

    Returns:
        dict: Converted dictionary

    Raises:
        Exception: If there is a problem with the file

    """
    from json import loads

    try:
        with open(json_path) as raw:
            jdict = loads(raw.read())
    except Exception as error:
        raise Exception("Problem with json file {}: {}".format(json_path, error))

    return jdict


# Aliases

def read_config(json_path: str) -> dict:
    """Read json file and returns dictionary.

    Args:
        json_path (str): Path to JSON file

    Returns:
        dict: Converted dictionary

    Raises:
        Exception: If there is a problem with the file

    """
    return json_to_dict(json_path)
# read_config = json_to_dict


def calculate_md5(file: str) -> str:
    """Calculate MD5 hash of local file."""
    md5_hash = hashlib.md5(open(file, 'rb').read())
    return md5_hash.hexdigest()


def path_md5(path: str) -> str:
    """Calculate MD5 hash of path."""
    md5_hash = hashlib.md5(path.encode())
    return md5_hash.hexdigest()
