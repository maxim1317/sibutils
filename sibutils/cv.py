"""Tools for cv jobs."""
import io
import os
import base64
import numpy as np
import cv2
import PIL
from PIL import Image, ImageDraw, ImageFont


_ROOT = os.path.abspath(os.path.dirname(__file__))


def _get_font_path(path: str) -> str:
    """Get path to font inside of the package.

    Args:
        path (str): relative path to font

    Returns:
        str: real path to font

    """
    return os.path.join(_ROOT, "assets", "fonts", path)


def get_writer(
        output_file: str,
        fps: int = 10,
        res_x: int = 1920,
        res_y: int = 1080
) -> cv2.VideoWriter:
    """Get MPEG videowriter.

    Args:
        output_file (str): file to write to
        fps (int, optional): fps of the video
        res_x (int, optional): width in pixels, defaults to 1920
        res_y (int, optional): height in pixels, defaults to 1080

    Returns:
        cv2.VideoWriter: videowriter

    """
    fourcc = cv2.VideoWriter_fourcc(*"MPEG")
    writer = cv2.VideoWriter(output_file, fourcc, fps, (res_x, res_y))
    return writer


def draw_prediction(image, text, color, bbox):
    """Draw bounding box and labels on images.

    Args:
        image (np.ndarray): Frame
        description (dict): Color and label
        bbox (dict): BBox

    Returns:
        np.ndarray: Frame with BBox

    """
    # bounding box drawing
    cv2.rectangle(
        image,
        (bbox["topleft"]["x"], bbox["topleft"]["y"]),
        (bbox["bottomright"]["x"], bbox["bottomright"]["y"]),
        color,
        4,
    )

    # annotation drawing
    cv2.putText(image, text, (bbox["topleft"]["x"], min(bbox["topleft"]["y"] + 20, image.shape[0])), cv2.FONT_HERSHEY_SIMPLEX,
                0.7, color, 2)
    return image


def bgr2rgb(bgr_image: np.ndarray) -> np.ndarray:
    """Convert BGR image to RGB."""
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
    return rgb_image


def rgb2bgr(rgb_image: np.ndarray) -> np.ndarray:
    """Convert RGB image to BGR."""
    bgr_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
    return bgr_image


def read_image(path: str) -> np.ndarray:
    """Read RGB image."""
    return cv2.imread(path)[:, :, ::-1]


def write_img(path: str, image: np.ndarray):
    """Write BGR image to disc."""
    cv2.imwrite(path, image)


def crop(image: np.ndarray, x: int, y: int, width: int, height: int) -> np.ndarray:
    """Crop image.

    Args:
        image (np.ndarray):
        x (int): x of the top-left corner of the crop
        y (int): y of the top-left corner of the crop
        width (int): width of the crop
        height (int): height of the crop

    Returns:
        np.ndarray: result

    """
    return image[y:y + height, x:x + width]


def np2pil(img_np: np.ndarray) -> PIL.Image:
    """Convert numpy image to PIL.Image."""
    temp_img = Image.fromarray(img_np)
    return temp_img


def b64_to_pil(base64_string: str) -> PIL.Image:
    """Convert base64 jpg to PIL.Image."""
    imgdata = base64.b64decode(base64_string)
    return Image.open(io.BytesIO(imgdata))


def np_to_b64(img: np.ndarray) -> bytes:
    """Convert numpy image to jpg in base64 encoding.

    Args:
        img (np.ndarray): image to convert

    Returns:
        bytes: jpg in base64 encoding

    """
    return jpg_as_text(img=img)


def put_russian(
        img: np.ndarray,
        text: str,
        position_x: int,
        position_y: int,
        color: tuple = (0, 0, 0),
        size: int = 20
) -> np.ndarray:
    """Put cyrillic text to image.

    Args:
        img (np.ndarray): target image
        text (str): text to put
        position_x (int): x coordinate from topleft corner
        position_y (int): y coordinate from topleft corner
        color (tuple, optional): text color, default to (0, 0, 0)
        size (int, optional): text size

    Returns:
        np.ndarray: image with text

    """
    print(_get_font_path("arial.ttf"))
    font_path = _get_font_path("arial.ttf")
    temp_img = Image.fromarray(img[:, :, ::-1])
    draw = ImageDraw.Draw(temp_img)
    draw.text((position_x, position_y), text, font=ImageFont.truetype(font_path, size), fill=color)
    result = np.array(temp_img)[:, :, ::-1]
    return result


def jpg_as_text(img: np.ndarray) -> bytes:
    """Convert numpy image to jpg in base64 encoding.

    Args:
        img (np.ndarray): image to convert

    Returns:
        bytes: jpg in base64 encoding

    """
    retval, buf = cv2.imencode('.jpg', img)

    if not retval:
        raise Exception('Could not convert image to JPG')

    jpg = base64.b64encode(buf)
    return jpg
