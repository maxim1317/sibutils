"""Tools for working with date and time."""
import datetime


def str_to_dt(date_string: str, date_format: str = '%m-%d-%Y %H:%M:%S') -> datetime.datetime:
    """Convert string to datetime.

    Args:
        date_string (str): string to convert
        date_format (str, optional): format of conversion, defaults to '%m-%d-%Y %H:%M:%S'

    Returns:
        datetime: converted datetime

    """
    converted_datetime = datetime.datetime.strptime(date_string, date_format)
    return converted_datetime


def dt_to_str(date: datetime.datetime, date_format: str = '%m-%d-%Y %H:%M:%S') -> str:
    """Convert datetime to string.

    Args:
        dt (datetime.datetime): datetime to convert
        date_format (str, optional): format of conversion, defaults to '%m-%d-%Y %H:%M:%S'

    Returns:
        str: String with date and time

    """
    string = date.strftime(date_format)
    return string
