"""Networking tools."""
import socket
from uuid import getnode


def get_mac() -> str:
    """Get host MAC."""
    mac = ':'.join(("%012X" % getnode())[i:i + 2] for i in range(0, 12, 2))
    return mac


def get_host() -> str:
    """Get hostname."""
    return socket.gethostname()

