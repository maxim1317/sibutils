"""Tools for cv visuals."""
import numpy as np
import cv2
from PIL import Image, ImageDraw, ImageFont


def draw_simple(
        image_bgr: np.ndarray,
        wcnt: int,
        zone_on: bool,
        zone_cnt: int,
        pose_on: bool,
        colors: dict
) -> np.ndarray:
    """Draws simple overlay with stats on the given image.

    Args:
        image_bgr (numpy.ndarray): BGR image
        wcnt                (int): number of found workers
        zone_on            (bool): indicates whether zone is on
        zone_cnt            (int): number of people standing in zone
        pose_on            (bool): indicates whether pose estimation is on
        colors             (dict): python dictionary with Sibintek colors

    Returns:
        numpy.ndarray: BGR image with drawn stats

    """
    out_img = image_bgr.copy()
    overlay = out_img.copy()
    alpha = 0.5

    im_h, im_w = out_img.shape[:2]

    # upper zone indicator background
    if zone_on:
        cv2.rectangle(overlay, (0, 0), (210, 30), colors["blue"][::-1], -1)
    # bottom pose indicator background
    if pose_on:
        cv2.rectangle(overlay, (0, im_h - 60), (30, im_h - 30), colors["blue"][::-1], -1)
    # bottom people counter background
    cv2.rectangle(overlay, (0, im_h - 30), (120, im_h), colors["blue"][::-1], -1)
    # cv2.drawContours(out_img, contours, -1, colors['yellow'][::-1], 2)
    cv2.addWeighted(overlay, alpha, out_img, 1 - alpha, 0, out_img)
    # border of bottom people counter
    cv2.rectangle(out_img, (0, im_h - 30), (120, im_h - 1), colors["lightblue"][::-1], 1)
    # upper zone indicator content
    if zone_on:
        cv2.rectangle(out_img, (0, 0), (210, 30), colors["lightblue"][::-1], 1)
        zone_status_str = "Busy" if (zone_cnt > 0) else "Clear"
        zone_color = colors["red"][::-1] if (zone_cnt) else colors["green"][::-1]
        cv2.putText(out_img, zone_status_str, (150, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, zone_color, 2)
        cv2.putText(
            out_img, "ZONE status: {}".format(""), (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, colors["white"][::-1], 1
        )
    # bottom pose indicator content
    if pose_on:
        cv2.rectangle(out_img, (0, im_h - 60), (30, im_h - 30), colors["lightblue"][::-1], 1)
        cv2.circle(out_img, (17, im_h - 45), 7, colors["blue"][::-1], -1)
        cv2.line(out_img, (5, im_h - 45), (17, im_h - 45), colors["yellow"][::-1], 2)
        cv2.circle(out_img, (17, im_h - 45), 7, colors["yellow"][::-1], 2)

    # bottom people counter content
    cv2.putText(out_img, "People: ", (5, im_h - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.7, colors["white"][::-1], 1)
    cv2.putText(out_img, str(wcnt), (90, im_h - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.7, colors["yellow"][::-1], 2)

    return out_img


def draw_sidebar(
        image_bgr: np.ndarray,
        wcnt: int,
        cask: int,
        zone_cnt: int,
        pose_on: bool,
        face_matched: bool,
        colors: dict,
        font_path: str,
        bg_img_path: str,
        width: int = 300
) -> np.ndarray:
    """Draws sidebar with detection stats on the right of width 300 and height of a given image.

    Args:
        image_bgr   (numpy.ndarray): BGR image
        wcnt                  (int): number of found workers
        cask                  (int): number of found heads without hardhats
        zone_cnt              (int): number of people standing in zone
        pose_on              (bool): indicates whether pose estimation is on
        face_matched         (bool): indicates whether a known person was found
        colors               (dict): python dictionary with Sibintek colors
        font_path             (str): path to a font(default is Arial)
        bg_img_path           (str): path to a bg image for sidebar
        width       (int, optional): width of a sidebar

    Returns:
        numpy.ndarray: RGB image with sidebar on the right

    """
    # create expanded image with 300px right sidebar
    imshape = (image_bgr.shape[1], image_bgr.shape[0])
    comb_size = (width + imshape[0], imshape[1])
    comb_img = Image.new("RGB", comb_size, color=colors["blue"])
    # convert image_bgr to RGB and paste to a bigger image
    comb_img.paste(Image.fromarray(image_bgr[:, :, ::-1]))
    # open and resize background for sidebar
    bottom_c = Image.open(bg_img_path)
    bg_size = bottom_c.size
    ratio = bg_size[1] / bg_size[0]
    bottom_c = bottom_c.resize((300, int(300 * ratio)), Image.ANTIALIAS)
    # dim background
    bottom_c = Image.eval(bottom_c, lambda x: x / 3)
    # paste to expanded image
    comb_img.paste(bottom_c, box=(imshape[0], 0))
    # creating draw object for recognition stats
    button_draw = ImageDraw.Draw(comb_img)
    # upper logo
    button_draw.rectangle(
        [imshape[0] + 20, 30, imshape[0] + 110, 50], fill=colors["yellow"], outline=colors["lightblue"]
    )
    sib_len = imshape[0] + 110 - imshape[0] - 20
    text_size = button_draw.textsize("СИБИНТЕК", font=ImageFont.truetype(font_path, 15))
    button_draw.text(
        (imshape[0] + 20 + sib_len // 2 - text_size[0] // 2, 32),
        "СИБИНТЕК",
        font=ImageFont.truetype(font_path, 15),
        fill=(0, 0, 0),
    )
    button_draw.rectangle([imshape[0] + 111, 30, comb_size[0] - 20, 50], outline=colors["lightblue"])
    store_len = comb_size[0] - 20 - imshape[0] - 111
    text_size = button_draw.textsize("ВИДЕОАНАЛИТИКА", font=ImageFont.truetype(font_path, 15))
    button_draw.text(
        (imshape[0] + 111 + store_len // 2 - text_size[0] // 2, 32),
        "ВИДЕОАНАЛИТИКА",
        font=ImageFont.truetype(font_path, 15),
        fill=colors["white"],
    )
    # people counter
    text_size = button_draw.textsize("Количество людей", font=ImageFont.truetype(font_path, 20))
    button_draw.text(
        (imshape[0] - text_size[0] // 2 + 150, 70),
        "Количество людей",
        font=ImageFont.truetype(font_path, 20),
        fill=colors["white"],
    )
    button_draw.rectangle(
        [imshape[0] + 1, 100, comb_size[0] - 1, 150], fill=colors["blue1"], outline=colors["lightblue"]
    )
    text_size = button_draw.textsize(str(wcnt), font=ImageFont.truetype(font_path, 45))
    button_draw.text(
        (imshape[0] + 150 - text_size[0] // 2, 100),
        str(wcnt),
        font=ImageFont.truetype(font_path, 45),
        fill=colors["yellow"],
    )
    # head without hardhat counter
    text_size = button_draw.textsize("Обнаружено без каски", font=ImageFont.truetype(font_path, 20))
    button_draw.text(
        (imshape[0] - text_size[0] // 2 + 150, 170),
        "Обнаружено без каски",
        font=ImageFont.truetype(font_path, 20),
        fill=colors["white"],
    )
    button_draw.rectangle(
        [imshape[0] + 1, 200, comb_size[0] - 1, 250], fill=colors["blue1"], outline=colors["lightblue"]
    )
    text_size = button_draw.textsize(str(cask), font=ImageFont.truetype(font_path, 45))
    button_draw.text(
        (imshape[0] + 150 - text_size[0] // 2, 200),
        str(cask),
        font=ImageFont.truetype(font_path, 45),
        fill=colors["yellow"],
    )
    # people in zone counter
    text_size = button_draw.textsize("Опасная зона", font=ImageFont.truetype(font_path, 20))
    button_draw.text(
        (imshape[0] - text_size[0] // 2 + 150, 270),
        "Опасная зона",
        font=ImageFont.truetype(font_path, 20),
        fill=colors["white"],
    )
    button_draw.rectangle(
        [imshape[0] + 1, 300, comb_size[0] - 1, 350], fill=colors["blue1"], outline=colors["lightblue"]
    )
    text_size = button_draw.textsize(str(zone_cnt), font=ImageFont.truetype(font_path, 45))
    button_draw.text(
        (imshape[0] + 150 - text_size[0] // 2, 300),
        str(zone_cnt),
        font=ImageFont.truetype(font_path, 45),
        fill=colors["yellow"],
    )
    # pose estimation is ON indicator
    text_size = button_draw.textsize("Обнаружение действий", font=ImageFont.truetype(font_path, 20))
    button_draw.text(
        (imshape[0] - text_size[0] // 2 + 150, 370),
        "Обнаружение действий",
        font=ImageFont.truetype(font_path, 20),
        fill=colors["white"],
    )
    button_draw.rectangle(
        [imshape[0] + 1, 400, comb_size[0] - 1, 450], fill=colors["blue1"], outline=colors["lightblue"]
    )
    pose_str = "Включено" if pose_on else "Выключено"
    pose_color = colors["green"] if pose_on else colors["red"]
    text_size = button_draw.textsize(pose_str, font=ImageFont.truetype(font_path, 45))
    button_draw.text(
        (imshape[0] + 150 - text_size[0] // 2, 400), pose_str, font=ImageFont.truetype(font_path, 45), fill=pose_color
    )
    # recognized face
    if face_matched:
        text_size = button_draw.textsize("Обнаружен:", font=ImageFont.truetype(font_path, 20))
        button_draw.text(
            (imshape[0] - text_size[0] // 2 + 150, 470),
            "Обнаружен:",
            font=ImageFont.truetype(font_path, 20),
            fill=colors["white"],
        )
        person_name = "Виктор Чикамасов"
        text_size = button_draw.textsize(person_name, font=ImageFont.truetype(font_path, 25))
        button_draw.text(
            (imshape[0] + 150 - text_size[0] // 2, 490),
            person_name,
            font=ImageFont.truetype(font_path, 25),
            fill=colors["yellow"],
        )
    # to return a BGR image
    comb_img = np.array(comb_img)[:, :, ::-1]
    return comb_img
