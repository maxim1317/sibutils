"""Tools for working with databases."""
import pymongo


def get_db(config: dict) -> pymongo.collection.Collection:
    """Get MongoDB client.

    Args:
        config (dict): host, port, name

    Returns:
        pymongo.MongoClient: database client

    """
    from pymongo import MongoClient

    port = config["port"]
    client = MongoClient(config["host"], port)
    database = client[config["db_name"]]

    return database
