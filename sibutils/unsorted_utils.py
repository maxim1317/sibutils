"""Here are functions that need sorting."""


def ensure_directory(directory: str) -> None:
    """Create directory if doesn't exist.

    Args:
        directory (str): path to check

    """
    import os

    if not os.path.exists(directory):
        os.makedirs(directory)
