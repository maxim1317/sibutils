"""Tools for working with loggers."""
from logging import Logger


def get_colored_logger(name: str, level: str = "INFO") -> Logger:
    """Return a logger with a default ColoredFormatter.

    Levels:
    NOTSET->DEBUG->INFO->WARNING->ERROR->CRITICAL

    Args:
        name (str): Logger name
        level (Optional[str], optional): logging level, defaults to INFO

    Returns:
        logging.Logger: Description

    """
    import logging as lg
    import coloredlogs

    logger = lg.getLogger(name)
    level = level.upper()

    if level == "INFO":
        logger.setLevel(lg.INFO)
    elif level == "DEBUG":
        logger.setLevel(lg.DEBUG)
    elif level == "WARNING":
        logger.setLevel(lg.WARNING)
    elif level == "ERROR":
        logger.setLevel(lg.ERROR)
    elif level == "CRITICAL":
        logger.setLevel(lg.CRITICAL)
    else:
        raise Exception("No such logging level")

    coloredlogs.install(level=level)

    return logger


# Aliases

def setup_logger(name: str, level: str = "INFO") -> Logger:
    """Return a logger with a default ColoredFormatter.

    Levels:
    NOTSET->DEBUG->INFO->WARNING->ERROR->CRITICAL

    Args:
        name (str): Logger name
        level (Optional[str], optional): logging level, defaults to INFO

    Returns:
        logging.Logger: Description

    """
    return get_colored_logger(name=name, level=level)
