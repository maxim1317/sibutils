"""Additional data is stored here."""
from sibutils.assets import fonts

__all__ = ['fonts']
