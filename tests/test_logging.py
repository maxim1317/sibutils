import pytest
from sibutils.logging import setup_logger


@pytest.mark.logging
class TestLogging:
    """Class for logging testing
    """

    def test_setup_logger(self):
        assert setup_logger(__name__) is not None
