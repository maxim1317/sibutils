FROM python:3.7

RUN apt-get install \
    bash \
    gcc

COPY test-reqs.txt .
RUN pip3 install --no-cache-dir -r test-reqs.txt

COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

WORKDIR /home
COPY . .

# RUN mkdir -p dist