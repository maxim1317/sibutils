from setuptools import setup

setup(
    name='sibutils',
    version='0.1',
    description='Utils for sibvision team',
    url='https://gl.grounddc.ru/sibvision/utils-library',
    author='sibvision',
    author_email='snesarevms@sibintek.ru',
    license='MIT',
    packages=['sibutils'],
    package_dir={'sibutils': 'sibutils'},
    package_data={'sibutils': ['assets/fonts/*.ttf', 'assets/fonts/*.otf']},
    include_package_data=True,
    install_requires=[
        'pytest',
        'coloredlogs',
        'typing',
        'pytest-cov',
        'pytest-sugar'
    ],
    zip_safe=False
)
